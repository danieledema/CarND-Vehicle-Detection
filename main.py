from sklearn.preprocessing import StandardScaler
from sklearn import svm
from sklearn.externals import joblib

import matplotlib
matplotlib.use('Agg')
from matplotlib import image, pyplot

import imageio
imageio.plugins.ffmpeg.download()
from moviepy.editor import VideoFileClip

from search import Window, MovingWindowSearcher, SimpleWindowGenerator

SCALER = joblib.load('scaler.p')
CLASSIFIER = joblib.load('classifier.p')

def pipeline(searcher, img):
    winds = searcher.getThings(img)
    print(len(winds))

    for w in winds:
        w.visualize(img)
    return img

def testImages():

    images = ['test1.jpg',
              'test2.jpg',
              'test3.jpg',
              'test4.jpg',
              'test5.jpg',
              'test6.jpg',
             ]

    for i in images:
        img = image.imread('test_images/' + i)
        
        print(i)

        searcher = MovingWindowSearcher(SCALER, CLASSIFIER)
        searcher.setWindowGenerator(SimpleWindowGenerator())

        img = pipeline(searcher, img)

        pyplot.imsave('output_images/' + i, img)


def testVideo():

    videos = ['test_video.mp4',
              'project_video.mp4',
             ]

    for v in videos:

        searcher = MovingWindowSearcher(SCALER, CLASSIFIER)
        searcher.setWindowGenerator(SimpleWindowGenerator())

        clip = VideoFileClip('test_videos/' + v)
        clip = clip.fl_image(lambda x: pipeline(searcher, x))
        clip.write_videofile('output_videos/' + v, audio=False)

if __name__ == '__main__':
    testImages()
    # testVideo()
