import cv2
import numpy as np
from skimage.feature import hog as skHog

def preprocess(img, size=(32, 32), nBins=32, orient=9, pix_per_cell=8, cell_per_block=2, vec=True):
    img = cv2.cvtColor(img, cv2.COLOR_RGB2YCrCb)

    featSpBin = spatialBin(img, size)
    featHist = hist(img, nBins)
    featHOG = hog(img, orient, pix_per_cell, cell_per_block, vec)

    return np.hstack([featSpBin, featHist, featHOG])

def spatialBin(img, size):
    return cv2.resize(img, size).ravel()

def hist(img, nBins):
    h = []
    for i in range(img.shape[2]):
        h.append(np.histogram(img[:, :, i], nBins)[0])
    return np.hstack(h)

def hog(img, orient=9, pix_per_cell=8, cell_per_block=2, vec=True):
    f = []
    for i in range(img.shape[2]):
        f.append(skHog(img[:, :, i],
                orientations = orient,
                pixels_per_cell = (pix_per_cell, pix_per_cell),
                cells_per_block = (cell_per_block, cell_per_block),
                block_norm = 'L2-Hys',
                visualise = False,
                transform_sqrt = False,
                feature_vector = vec))
    return np.hstack(f)
