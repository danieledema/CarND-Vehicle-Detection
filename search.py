import cv2
import numpy as np

from preprocessing import preprocess

SHAPE = (720, 1280)

class Window:
    def __init__(self, p1, p2):
        self.p1 = tuple(p1.astype(np.int64))
        self.p2 = tuple(p2.astype(np.int64))

    def visualize(self, img, color=(0, 0, 255), thickness=4):
        cv2.rectangle(img, self.p1, self.p2, color, thickness)

class SimpleWindowGenerator:
    def __init__(self):
        pass

    def __call__(self, img):

        self.windows = []

        self.windows.append(Window(np.array([830, 400]), np.array([930, 500])))

        # winLen = [60, 90, 110, 150]
        # for l in winLen:
        #     p1 = np.array([0, SHAPE[0]/2])
        #     p2 = p1 + l

        #     while p2[1] <= SHAPE[0]:
        #         self.windows.append(Window(p1, p2))

        #         p1 += np.array([l/2, 0])
        #         p2 += np.array([l/2, 0])

        #         if p2[0] > SHAPE[1]:
        #             p1 += np.array([0, l/2])
        #             p1[0] = 0
        #             p2 += np.array([0, l/2])
        #             p2[0] = l

        # print(len(self.windows))

        for w in self.windows:
            yield w

class MovingWindowSearcher:
    def __init__(self, scaler, classifier):
        self.scaler = scaler
        self.classifier = classifier

    def setWindowGenerator(self, wg):
        self.windGen = wg

    def findOccurrences(self, img):
        for w in self.windGen(img):
            if self.isThereAThing(img, w):
                yield w

    def isThereAThing(self, img, w):
        x = img.copy()
        x = x[w.p1[1]:w.p2[1], w.p1[0]:w.p2[0], :]
        x = cv2.resize(x, (64, 64))
        x = preprocess(x).reshape(1, -1)
        x = self.scaler.transform(np.array(x))

        if self.classifier.predict(x) == 1:
            return True
        return False

    def getThings(self, img):
        winds = []
        for w in self.findOccurrences(img):
            winds.append(w)
        return winds
