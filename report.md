[//]: # (Image References)
[image1]: ./examples/car_not_car.png
[image2]: ./examples/HOG_example.jpg
[image3]: ./examples/sliding_windows.jpg
[image4]: ./examples/sliding_window.jpg
[image5]: ./examples/bboxes_and_heat.png
[image6]: ./examples/labels_map.png
[image7]: ./examples/output_bboxes.png
[video1]: ./project_video.mp4

## Vehicle Detection Project

### Preprocessing and Feature Extraction

The code for this section can be found in the "preprocessing.py" script.

After experimenting with the color spaces, I chose the YCrCb color space for the color histogram features.
I then combined the histogram features with spatial bins and hog features.

### Classifier and training

The code for this section can be found in the "train.py" script.
I decided to use a StandardScalar scalar and SVM classifier.
The two are trained using the images from the dataset, after being preprocessed.

The two are trained and saved in external files: "scalar.p" and "classifier.p", in order to be used without the necessity of training them every time.
I decided to use "joblib" instead of "pickle" because of the optimization towards the numpy structures.

I had an accuracy of over 98%.
