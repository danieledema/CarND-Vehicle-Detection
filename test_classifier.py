from sklearn.preprocessing import StandardScaler
from sklearn import svm
from sklearn.externals import joblib

import cv2

import matplotlib
matplotlib.use('Agg')
from matplotlib import image, pyplot

from search import Window, MovingWindowSearcher, SimpleWindowGenerator
from preprocessing import *

SCALER = joblib.load('scaler.p')
CLASSIFIER = joblib.load('classifier.p')

img = image.imread('test_images/test1.jpg')
# img = image.imread('training_set/vehicles/GTI_Right/image0003.png')
img = img[420:530, 820:930]
img = img.copy()

img = cv2.resize(img, (64, 64))
pyplot.imsave('tested.jpg', img)

img = preprocess(img).reshape(1, -1)
img = SCALER.transform(img)
print(CLASSIFIER.predict(img))
print(CLASSIFIER.decision_function(img))
