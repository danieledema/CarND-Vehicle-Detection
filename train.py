import glob
import matplotlib.image as image
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn import svm
from sklearn.externals import joblib

from preprocessing import preprocess

def trainSVM(x_train, y_train, x_test, y_test):
    classif = svm.SVC(kernel='rbf', C=10)
    classif.fit(x_train, y_train)
    acc = classif.score(x_test, y_test)

    return classif, acc

if __name__ == '__main__':

    print('Loading images')
    cars = glob.glob('./training_set/vehicles/*/*')
    non_cars = glob.glob('./training_set/non-vehicles/*/*')
    print('Found {} car and {} non car images'.format(len(cars), len(non_cars)))

    car_imgs = []
    for i in cars[:100]:
        car_imgs.append(image.imread(i))

    non_car_imgs = []
    for i in non_cars[:100]:
        non_car_imgs.append(image.imread(i))

    print('Images loaded')

    car_feat = []
    while len(car_imgs) > 0:
        car_feat.append(preprocess(car_imgs.pop()))
    print('Car images preprocessed')

    non_car_feat = []
    while len(non_car_imgs) > 0:
        non_car_feat.append(preprocess(non_car_imgs.pop()))
    print('Non car images preprocessed')

    print('Images preprocessed')

    x = np.vstack((car_feat, non_car_feat))
    y = np.hstack((np.ones(len(car_feat)), np.zeros(len(non_car_feat))))

    print('Scaling')

    scaler = StandardScaler().fit(x)
    joblib.dump(scaler, 'scaler.p')
    x = scaler.transform(x)

    print('Scaler saved')

    rand_state = np.random.randint(0, 100)
    x_train, x_test, y_train, y_test = train_test_split(x,
                                                        y,
                                                        test_size=0.1,
                                                        random_state=rand_state)

    classifier, accuracy = trainSVM(x_train, y_train, x_test, y_test)

    print('Got an accuracy of {}'.format(accuracy))
    joblib.dump(classifier, 'classifier.p')
    print('Classifier saved')
